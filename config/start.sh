#!/bin/sh

cd /go/src

# git clone on deploy
#if [ ! -d app ] ; then
  echo "cloning app repository..."
  mkdir -p /go/bulk/bca/inbox
  mkdir -p /go/bulk/bca/outbox
  git clone ${GIT_REPOSITORY} app
  cd app
  echo "installing dependencies..."
  ls -al
  go get
  go build 
  echo "running go app..."
  ./app
#fi